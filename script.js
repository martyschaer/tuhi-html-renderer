const canvas_w = 740
const canvas_h = 1053
const factor = 100

const canvas = document.getElementById('canvas')

const ctx = canvas.getContext('2d')
const colorInput = document.getElementById('colorpicker')
const renderButton = document.getElementById('button_render')
renderButton.onclick = loadFile
const animateCheckbox = document.getElementById('checkbox_animate')
const randcolorCheckbox = document.getElementById('checkbox_randcolor')

canvas.width = canvas_w
canvas.height = canvas_h

async function render(data) {
    let animate = animateCheckbox.checked
    canvas.height = data.dimensions[0] / factor
    canvas.width = data.dimensions[1] / factor
    ctx.strokeStyle = getInputColor()
    for (const stroke of data.strokes) {
        await draw(stroke, animate)
        if (animate) {
            await sleep(50)
        }
    }
}

async function draw(stroke, animate) {
    if (randcolorCheckbox.checked) {
        ctx.strokeStyle = getRandomColor()
    }
    ctx.beginPath();
    var prev = xform(stroke.points[0])
    for (const point of stroke.points) {
        ctx.beginPath()
        let pos = xform(point)
        ctx.moveTo(prev.x, prev.y)
        ctx.lineTo(pos.x, pos.y)
        ctx.lineWidth = point.pressure / 10000
        ctx.stroke()
        ctx.closePath();
        prev = xform(point)
        if (animate) {
            await sleep(0)
        }
    }
}

function xform(point) {
    let position = point.position
    let x = canvas.width - position[1] / factor
    let y = position[0] / factor
    return { 'x': x, 'y': y }
}

/* From: https://stackoverflow.com/a/1484514/2941551 */
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getInputColor() {
    return colorInput.value
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/* From: https://stackoverflow.com/a/21446426/2941551 */
function loadFile() {
    var input, file, fr;

    if (typeof window.FileReader !== 'function') {
        alert("The file API isn't supported on this browser yet.");
        return;
    }

    input = document.getElementById('input_file');
    if (!input) {
        alert("Couldn't find the fileinput element.");
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
    }
    else {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
    }

    async function receivedText(e) {
        let lines = e.target.result;
        var data = JSON.parse(lines);
        render(data)
    }
}